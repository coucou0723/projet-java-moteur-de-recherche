package com.mycompany.mavenproject2;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *  la classe de Lit schema RDF
 * Trouve la relation entre les mots cles
 * Lance le warpper qui correspend a la classe du mot
 */

public class Model {
    private final OntModel model;
    private final String NAMESPACE="http://www.uvsq.com/#";
    private Map<String,String> urlForWarrper;
    private final ParseurXml resource;

    private ListeResultat listeresultat;

    public ListeResultat getListeresultat() {
        ListeResultat r=new ListeResultat();
        r.ajouelist( listeresultat);
        listeresultat.clear();
        return r;
    }
   
   /**
    * Constructeur on lit le fichier rdf
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws IOException
    * @throws ClassNotFoundException 
    */
    public Model() throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException
    {   model = ModelFactory.createOntologyModel();
        resource=new ParseurXml();
        urlForWarrper = new HashMap();
        listeresultat= new ListeResultat();
        model.read(new File("schemaRDF.owl").toURI().toString());
  
  }

    public OntModel getModel() {
        return model;
    }
    /***
     *  Lance le cas d un mot simple ou compose
     * @param mesClasse les classe des motd cles
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
  public void recherche(Map<String,String> mesClass) throws ParserConfigurationException, SAXException, IOException
  {  
      if (mesClass.size()==1)
         {   
            for( String key:mesClass.keySet())
              MotSimple(mesClass.get(key), key);
         }
         else {
      if(mesClass.size()==2)
      {
 OntClass maclasse1=null,maclasse2=null;
        String motcle1=null,motcle2=null;
       
        int i=0;
     for(String key :  mesClass.keySet() )
     { System.out.println(key+"  "+mesClass.get(key));
         if(i==0) { motcle1=key; maclasse1=model.getOntClass(NAMESPACE+mesClass.get(key));  }
     else  { motcle2=key; maclasse2=model.getOntClass(NAMESPACE+mesClass.get(key));  
     }
     i++;
      }
          Mot_Compose(maclasse1,motcle1,maclasse2,motcle2);
      
      }
      }

  }
  
 /**
  * retourne la classe qui corespond a la relation des deux classes
  * @param classe1
  * @param classe2
  * @return 
  */ 
  public OntClass getRelationClasse (OntClass classe1,OntClass classe2)
  { OntClass superclasse=null;
 OntProperty property=null;
      ExtendedIterator<OntProperty> listeProperty=classe1.listDeclaredProperties();
     while (listeProperty.hasNext())
     {  property=listeProperty.next();
         if(property.getRange().asClass().equals(classe2))
     {
          superclasse=classe2;
     }
         
     }
     if(superclasse==null)
     {
         listeProperty=classe2.listDeclaredProperties();
     while (listeProperty.hasNext())
     {  property=listeProperty.next();
         if(property.getRange().asClass().equals(classe1))
     {
          superclasse=classe1;
     }
         
     }
     }
  return superclasse;
  }
  /**
   * Retourne la classe super classe si la relation entre les deux classe si l'heritage
   * @param classe1
   * @param classe2
   * @return 
   */ 
  public OntClass getSuperClasse (OntClass classe1,OntClass classe2)
  { OntClass superclasse=null;
 OntClass classe=null;
      ExtendedIterator<OntClass> supers=classe1.listSuperClasses();
     while (supers.hasNext())
     {  classe=supers.next();
         if(classe.equals(classe2))
     {
          superclasse=classe2;
     }
         
     }
     if(superclasse==null)
     {
         supers=classe1.listSubClasses();
     while (supers.hasNext())
     {  
         classe=supers.next();
         if(classe.equals(classe2))
     {
          superclasse=classe1;
     }
         
     }
     }
  return superclasse;
  }
  
   /**
    * trouve la resource a chercher 
    * @param classeMot
    * @param motCle
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws IOException 
    */ 
     public void MotSimple(String classeMot,String motCle) throws ParserConfigurationException, SAXException, IOException
  { urlForWarrper.clear();
  urlForWarrper.putAll(getUrl(motCle.toUpperCase(),classeMot,classeMot));
 if(urlForWarrper.size()==1) ChoisirWarrperSource(classeMot,classeMot,motCle);
 else { 
  
     OntClass motclass= model.getOntClass(NAMESPACE+classeMot);
             ExtendedIterator<OntClass> supers=motclass.listSuperClasses();
      while (supers.hasNext())
      {
          OntClass supclass= supers.next();
          if(supclass.getLocalName().equals("Thing"))
          {
              ExtendedIterator<OntProperty> listeproprties=motclass.listDeclaredProperties();
              while(listeproprties.hasNext())
              { OntProperty proprety=listeproprties.next();
                  System.out.println("TAHER Yehia");
                  ChoisirWarrperSource(classeMot, proprety.getRange().asClass().getLocalName(), motCle);
            
              }
           }
          else    ChoisirWarrperSource(classeMot, supclass.getLocalName(), motCle);
             
              
}
 }
 }
  /**
   * lance et choisi la resource a recherche
   * @param classeMot
   * @param nomResource
   * @param motCle
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException 
   */
  public void ChoisirWarrperSource (String classeMot,String nomResource,String motCle) throws ParserConfigurationException, SAXException, IOException
  { 
  switch (nomResource)
          { case "Formation": 
              urlForWarrper.clear();
              urlForWarrper.putAll(getUrl(motCle.toUpperCase(),nomResource,classeMot));
             WarpperFormation warpperformation= new  WarpperFormation();
                 warpperformation.finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
            listeresultat.ajouelist(warpperformation.getListresultat());
             break;
                  case "Composante":
                  urlForWarrper.clear();    
                  urlForWarrper.putAll(getUrl(motCle.toUpperCase(),nomResource,classeMot));
              WarrperUfrDesSiences warrperUfr=new WarrperUfrDesSiences();
                     warrperUfr.finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
            listeresultat.ajouelist(warrperUfr.getListresultat());  
                  break;
                      case "laboratoire":
                  urlForWarrper.clear();    
                  urlForWarrper.putAll(getUrl(motCle.toUpperCase(),nomResource,classeMot));
                          System.out.println("walo");
              WarrperUfrDesSiences warrperUfr1=new WarrperUfrDesSiences();
                        warrperUfr1.finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
            listeresultat.ajouelist(warrperUfr1.getListresultat());  
                  break;
                  case "emploi_du_temps":
                      urlForWarrper.clear();
                       urlForWarrper.putAll(getUrl(motCle.toUpperCase(),nomResource,classeMot));
                 WarperEmploiDuTemps warrperEmploi =new  WarperEmploiDuTemps();
                    warrperEmploi.finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
            listeresultat.ajouelist(warrperEmploi.getListresultat());  
                      // new WarperEmploiDuTemps().finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
                      break;

          }
  
  
  }
  /**
   * lance le warrper corespend a le nom de la resource
   * @param classeMot
   * @param nomResource
   * @param motCle
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException 
   */
    public void ChoisirWarrper (String classeMot,String nomResource,String motCle) throws ParserConfigurationException, SAXException, IOException
  { 
  switch (nomResource)
          { case "Formation": 
                  WarpperFormation warpperformation= new  WarpperFormation();
                    warpperformation.finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
            listeresultat.ajouelist(warpperformation.getListresultat());
             break;
                  case "Composante":
                 WarrperUfrDesSiences warrperUfr=new WarrperUfrDesSiences();
                        warrperUfr.finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
            listeresultat.ajouelist(warrperUfr.getListresultat());  
            break;
                  case "Emploi_du_temps":
                        WarperEmploiDuTemps warrperEmploi =new  WarperEmploiDuTemps();
                    warrperEmploi.finder(urlForWarrper,classeMot.toLowerCase() , motCle.toUpperCase());
            listeresultat.ajouelist(warrperEmploi.getListresultat());  
            break;

          }
  
  
  }
     /**
      * cherche la relation entre les deux mots et lance le warrper
      * @param maclasse1 
      * @param motcle1
      * @param maclasse2
      * @param motcle2
      * @throws IOException
      * @throws ParserConfigurationException
      * @throws SAXException 
      */
  public void Mot_Compose(OntClass maclasse1,String motcle1,OntClass maclasse2,String motcle2) throws IOException, ParserConfigurationException, SAXException
  { 
OntClass superclasse=getSuperClasse(maclasse1, maclasse2);
  boolean isHeritage=true;
 if(superclasse==null)  {superclasse=getRelationClasse(maclasse1, maclasse2); isHeritage=false;}
 else if(isHeritage) {
if(maclasse1.equals(superclasse) && maclasse2.getLocalName().equalsIgnoreCase(motcle2)) isHeritage=false;
if(maclasse2.equals(superclasse) && maclasse1.getLocalName().equalsIgnoreCase(motcle1)) isHeritage=false;

}
    if(superclasse!=null)
    {
        if(!isHeritage)
        { 
     Map<String,String> map=getUrlcompose(superclasse.getLocalName());

     if(maclasse1.getLocalName().equalsIgnoreCase(motcle1))
     { 
     if(!superclasse.equals(maclasse1)) {
         urlForWarrper.clear();
            urlForWarrper.put(motcle2.toUpperCase(), map.get(motcle2.toUpperCase()).toString());
             ChoisirWarrper(maclasse1.getLocalName(),superclasse.getLocalName(), motcle1.toUpperCase()); 
     }
     else  {
         urlForWarrper.clear();
         urlForWarrper.putAll(map);
     ChoisirWarrper(maclasse2.getLocalName(),superclasse.getLocalName(), motcle2.toUpperCase());
     }
    // System.out.println(map.size());
         
     }
     
     else {  
      if(maclasse2.getLocalName().equalsIgnoreCase(motcle2))
     { 
     if(!superclasse.equals(maclasse2)) { /******* on cherche le 2 eme mot dans tous les page****************/ 
         urlForWarrper.clear();  
         urlForWarrper.put(motcle1.toUpperCase(), map.get(motcle1.toUpperCase()).toString());
       ChoisirWarrper(maclasse2.getLocalName().toLowerCase(),superclasse.getLocalName(), motcle2.toUpperCase());          
     }
     else  {
         urlForWarrper.clear();
         urlForWarrper.putAll(map);
       ChoisirWarrper(maclasse1.getLocalName().toLowerCase(),superclasse.getLocalName(), motcle1.toUpperCase());
     }
      
     }  else { int motAchercher=0;  
          
          if(!superclasse.equals(maclasse2)) { 
              urlForWarrper.clear();
            urlForWarrper.put(motcle1.toUpperCase(), map.get(motcle1.toUpperCase()).toString());
            motAchercher=2;
                }
     else  { 
     urlForWarrper.clear();
              urlForWarrper.put(motcle2.toUpperCase(), map.get(motcle2.toUpperCase()).toString());  motAchercher=1;}
          for (String kk:urlForWarrper.keySet())
          { System.out.println(urlForWarrper.get(kk));}
          
          if(motAchercher==1)
                      ChoisirWarrper(maclasse1.getLocalName(), superclasse.getLocalName(), motcle1.toUpperCase());   
                  else  ChoisirWarrper(maclasse2.getLocalName(), superclasse.getLocalName(), motcle2.toUpperCase());
              }
  
     }
         
     }else { /********* cas hetage on elimine la classe fils*/
         
        if(maclasse1.equals(superclasse))
        {  if(maclasse1.getLocalName().equalsIgnoreCase(motcle1))
        { MotSimple(maclasse2.getLocalName(), motcle2); }
        else {      MotSimple(maclasse1.getLocalName(), motcle1);}  }
        else {if(maclasse2.getLocalName().equalsIgnoreCase(motcle2))
        {  MotSimple(maclasse1.getLocalName(), motcle1); }
        else {      MotSimple(maclasse2.getLocalName(), motcle2);}  }
        
        }  
        
    } 
        else { if(maclasse1.equals(maclasse2))
        { if(maclasse1.getLocalName().equalsIgnoreCase(motcle1)) MotSimple(maclasse2.getLocalName(),motcle2);
        else { if(maclasse2.getLocalName().equalsIgnoreCase(motcle2)) MotSimple(maclasse1.getLocalName(),motcle1);
        
        else  MotSimple(maclasse1.getLocalName(),motcle1);}
        
        }
        else   listeresultat.ajoue(new Resultat("aucune relation entre ", motcle2, motcle2));}
    
          
  
     
  
  }
  
  /**
   *  retourne liste complet des url crospendant a la resource
   * @param nomSuperClasse
   * @return
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException 
   */
  public Map getUrlcompose(String nomRessource) throws ParserConfigurationException, SAXException, IOException
  {
      Map<String,String> map=new HashMap();
     
  switch(nomRessource)
  { case "Formation": 
       map=resource.getFormationmap();
     break;
    case "Composante":
        map=resource.getSiencenmap();
        break;
        case "Departement":
        map=resource.getSiencenmap();
        break;
        case "laboratoire":
        map=resource.getSiencenmap();
        break;
    case "Emploi_du_temps": 
        map=resource.getemploimap();
        break;
  
  }
  return map;
  }
  /****
   * 
   * retourne liste des urls de la resource correspandant a le mot cle
   * @param nom
   * @param superclass
   * @param classe
   * @return
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException 
   */
  public Map getUrl(String nom , String superclass,String classe) throws ParserConfigurationException, SAXException, IOException
  { 
      Map map=new HashMap();
      if(classe.equals("Departement") || classe.equals("laboratoire"))
          map.put(nom,resource.getSiencenmap().get(nom));
      else {
      if(superclass.equals("Formation"))
      if(superclass.equals(classe))
      {map.put(nom,resource.getFormationmap().get(nom));}
      else {
         return resource.getFormationmap();
      }
       if(superclass.equals("Composante"))
      if(superclass.equals(classe))
      { map.put(nom,resource.getSiencenmap().get(nom)); }
      else {
         return resource.getSiencenmap();
      }
      
        if(superclass.equals("Emploi_du_temps"))
      if(superclass.equals(classe))
      {map.put(nom,resource.getemploimap().get(nom));}
      else {
         return resource.getemploimap();
      }
      }      
     return map;
  }
}
