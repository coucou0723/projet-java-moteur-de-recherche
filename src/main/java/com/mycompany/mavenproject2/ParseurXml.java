package com.mycompany.mavenproject2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *Parseur xml Pour lire un fichier xml 
 * qui contient notre lien de ressource
 */
public class ParseurXml {
 private Element racine;
    public ParseurXml() throws ParserConfigurationException, SAXException, IOException {
        String fichier="ressource.xml";
		//File f= new File(fichier);
		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder= factory.newDocumentBuilder();
		Document document =builder.parse(fichier);
	racine= document.getDocumentElement();
    }
    /**
     * 
     * @return liste des liens du catalogue formation
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    
    public Map getFormationmap() throws ParserConfigurationException, SAXException, IOException
	{
		Map<String,String>  map= new HashMap();
                String url = null;
		String name = null;
		NodeList bases=racine.getElementsByTagName("resourceF");
                
		for(int i=0; i<bases.getLength();i++)
		{ Node base=bases.item(i);
               
		NodeList elemts=base.getChildNodes();
		for (int j=0;j<elemts.getLength(); j++)
		{
			Node action =elemts.item(j);
                      //  System.err.println(action.getNodeName());
			if(action.getNodeName().equals("name"))
			name=action.getTextContent().toUpperCase();	
			if(action.getNodeName().equals("url"))
				url=action.getTextContent();
		}
			map.put(name,url);
			
			
		}
		return map;
	}
    /**
     * 
     * @return liste des liens des departements et laboratoires de l'UFR des sciences
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public Map getSiencenmap() throws ParserConfigurationException, SAXException, IOException
	{
		Map<String,String>  map= new HashMap();
                String url = null;
		String name = null;
		NodeList bases=racine.getElementsByTagName("resourceUfr");
                
		for(int i=0; i<bases.getLength();i++)
		{ Node base=bases.item(i);
               
		NodeList elemts=base.getChildNodes();
		for (int j=0;j<elemts.getLength(); j++)
		{
			Node action =elemts.item(j);
                      //  System.err.println(action.getNodeName());
			if(action.getNodeName().equals("name"))
			name=action.getTextContent().toUpperCase();	
			if(action.getNodeName().equals("url"))
				url=action.getTextContent();
		}
			map.put(name,url);
			
			
		}
		return map;
	}
    /**
     * 
     * @return les liens des emplois du temps
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public Map getemploimap() throws ParserConfigurationException, SAXException, IOException
	{
		Map<String,String>  map= new HashMap();
                String url = null;
		String name = null;
		NodeList bases=racine.getElementsByTagName("resource");
                
		for(int i=0; i<bases.getLength();i++)
		{ Node base=bases.item(i);
               
		NodeList elemts=base.getChildNodes();
		for (int j=0;j<elemts.getLength(); j++)
		{
			Node action =elemts.item(j);
                      //  System.err.println(action.getNodeName());
			if(action.getNodeName().equals("name"))
			name=action.getTextContent().toUpperCase();	
			if(action.getNodeName().equals("url"))
				url=action.getTextContent();
		}
			map.put(name,url);
			
			
		}
		return map;
	}
}
