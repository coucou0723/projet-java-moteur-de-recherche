package com.mycompany.mavenproject2;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 *
 * 
 */
public class RechercheLocal {
    
    public String getClass(String unMotCle, OntModel myModel)
          {  
  String saClasse=null;
  boolean trouve=false;
   ExtendedIterator<Individual> individuals =  myModel.listIndividuals();
   ExtendedIterator<OntClass> listClass =  myModel.listClasses();
    while (listClass.hasNext() && !trouve) {
        OntClass ontClass = listClass.next();
       
     if(unMotCle.toLowerCase().equals(ontClass.getLocalName().toLowerCase())){
        saClasse= ontClass.getLocalName();
               trouve=true;
       }
       }
   while (individuals.hasNext()&& !trouve) {
      Individual individual = individuals.next();
      String individu = individual.getLocalName();
  if(unMotCle.toLowerCase().equals(individu.toLowerCase())){
               saClasse= individual.getOntClass().getLocalName();
               trouve=true;
                }
    }
   return saClasse;
  }
    
}
