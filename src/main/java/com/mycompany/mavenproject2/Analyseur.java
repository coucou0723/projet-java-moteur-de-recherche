package com.mycompany.mavenproject2;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * cette classe comporte 
 */
public class Analyseur {
    private OntModel myModel;
public Analyseur()
  {      
      myModel = ModelFactory.createOntologyModel();
        try{  
            myModel.read(new File("Donnees.owl").toURI().toString());
         }
   catch (Exception E)
   { E.printStackTrace(); }
    }

    public OntModel getModel() {
        return myModel;
    }

  public  Map <String,String>  getListClass(String unMotCle, Map <String,String> listAbreviation) throws IOException{
     unMotCle= unMotCle.replace(" ","_");
     unMotCle= unMotCle.replace("'","");
     Map <String,String>  sesClass = new HashMap<String,String>();
     String maPhrase="";
     String saClasse;
     String sonAbreviation;
     RechercheLocal rechercheLocal = new RechercheLocal();
     
     saClasse=rechercheLocal.getClass(unMotCle,myModel);
     
      if(saClasse!=null){
          
              sonAbreviation = getAbriviation(unMotCle,listAbreviation);
            //  System.out.println("ssqdqs");
              if(sonAbreviation!=null) sesClass.put(sonAbreviation,saClasse);
              
               else  { sesClass.put(unMotCle,saClasse);}
       maPhrase="";
       }
      else{
         
            RechercheEnLigne rechercheEnLigne = new RechercheEnLigne();
            saClasse = rechercheEnLigne.getClassePersonne(unMotCle);
             System.out.println("kael");
           if(saClasse!=null)  sesClass.put(unMotCle,saClasse);
           else{
              String[] tabCle = unMotCle.split("_");
              int i=0;
               while(i<tabCle.length){
                if(maPhrase=="") maPhrase= maPhrase+(tabCle[i]);
                else maPhrase= maPhrase+"_"+(tabCle[i]);
                   saClasse = rechercheLocal.getClass(maPhrase,myModel); 
                  if(saClasse!=null){
                    sonAbreviation = getAbriviation(maPhrase,listAbreviation);
                    if(sonAbreviation!=null) sesClass.put(sonAbreviation,saClasse);
                    else  sesClass.put(maPhrase,saClasse);
                 maPhrase="";
                  }
               i++;
              }
           }
        }
   return sesClass;
  }
 
  public String getAbriviation(String unePhrase, Map<String,String> listAbreviation){
      String sonAbreviation = null;
     if(listAbreviation.containsKey(unePhrase)== true)
        sonAbreviation = listAbreviation.get(unePhrase);
     
    return sonAbreviation;   
  } 
 
  }
